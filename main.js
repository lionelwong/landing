(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-19593198-1', 'auto');
ga('send', 'pageview');

function openModal(id, name) {
  // name expected is <institution slug> + '.html', or <institution slug> + '.' + <language code> + '.html'
  // e.g. 'unitar-international-university-11409.html' or 'unitar-international-university-11409.id.html'

  // URL_HEAD expected is e.g. 'www.easyuni.com/malaysia/' declared in index.html

  name_split = name.split('.');
  if (name_split.slice(-1)[0] == 'html') {
    ga_url = URL_HEAD + name_split[0] + '/virtual';
    console.log(ga_url);
    ga('send', 'pageview', ga_url);
    document.getElementById(id).src = '../institutions/' + name;
  }
}
